import {mainData as data} from './data.js';

function TransformData(){
    
    let result = data.map((element) => {
        var group = element.group;
        group.map((item) => {
          delete item.brand;
          delete item.category;
          delete item.thumbnail;
          delete item.name;
          delete item.nameExtended;
          delete item.rankOrganic;
          //console.log(item)
          return item;
        });
        element.group = group;
        return element;
      });
      return result;
    
}

let dataForTemplate = TransformData()
console.log(dataForTemplate)

dataForTemplate.map(item=>{
    item.group.map(obj=>{
        
        if(obj && obj.price && obj.price[0] && obj.price[0] !== false && obj.price[0] != "" && obj.price[0] !== undefined ){
            let price = obj.price[0];
            let productUrl = obj.productUrl[0];
            let URI = `https://www.metro.ca` + `${productUrl.text}`;
            let rank = obj.rank[0];
            

            if(price.text != "" && price.text != undefined ){
                let {text} = price;
            
                let formated = text.match(/[+-]?\d+(\.\d+)?/g)[0]
                
                document.getElementById('tag-id').innerHTML +=`<li>${formated}</li>`;
            }
            if(productUrl.text != "" && productUrl.text != undefined){
                document.getElementById('tag-id').innerHTML +=`<li>${URI}</li>`;
                
            }
            if(rank.text != "" && rank.text != undefined){
                document.getElementById('tag-id').innerHTML +=`<li>${rank.text}</li>`;
            }
        }
    })
});





// let text2 = ' price 1.95 euro';
// var thenum = text2.match(/[+-]?\d+(\.\d+)?/g)[0];
// console.log(thenum);